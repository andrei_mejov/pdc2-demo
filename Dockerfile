FROM openjdk:8u171

EXPOSE 8080

ENV USER_NAME=pdc2

RUN useradd -ms /bin/sh $USER_NAME

RUN apt-get update && apt-get install -y redis-server
RUN apt-get install -y mongodb

#RUN mkdir /opt/mongodb
#RUN cd /opt && chown -R pdc2:pdc2 ./mongodb

RUN mkdir /opt/pdc2-demo
COPY ./dist/ /opt/pdc2-demo
RUN cd /opt/pdc2-demo && chown -R pdc2:pdc2 ./
RUN cd /opt/pdc2-demo && ls -l

USER pdc2
CMD /bin/sh -c "/usr/bin/redis-server &" && \
    mkdir /tmp/mongodb && \
    mkdir /tmp/hsql && cp /opt/pdc2-demo/demo/data/hsql/* /tmp/hsql/ && \
    cp /opt/pdc2-demo/demo/data/TechCrunchcontinentalUSA.xlsx /tmp/ && \
    /bin/sh -c "/usr/bin/mongod --dbpath /tmp/mongodb &" && \
     cd /opt/pdc2-demo && \
     ./pdc2 web 8080